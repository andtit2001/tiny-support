#include <gtest/gtest.h>

#include <tinysupport/result.hpp>

using namespace tinyfiber;

////////////////////////////////////////////////////////////////////////////////

static std::error_code TimedOut() {
  return std::make_error_code(std::errc::timed_out);
}

////////////////////////////////////////////////////////////////////////////////

class TestClass {
 public:
  TestClass(std::string message)
    : message_(std::move(message)) {
    ++object_count_;
  }

  TestClass(const TestClass& that)
    : message_(that.message_) {
  }

  TestClass& operator =(const TestClass& that) {
    message_ = that.message_;
    return *this;
  }

  TestClass(TestClass&& that)
    : message_(std::move(that.message_)) {
  }

  TestClass& operator =(TestClass&& that) {
    message_ = std::move(that.message_);
    return *this;
  }

  ~TestClass() {
    --object_count_;
  }

  const std::string& Message() const {
    return message_;
  }

  static int ObjectCount() {
    return object_count_;
  }

 private:
  TestClass();

 private:
  std::string message_;
  static int object_count_;
};

int TestClass::object_count_ = 0;

////////////////////////////////////////////////////////////////////////////////

Result<std::vector<int>> MakeVector(size_t size) {
  std::vector<int> ints;
  ints.reserve(size);
  for (size_t i = 0; i < size; ++i) {
    ints.push_back(i);
  }
  return make_result::Ok(std::move(ints));
}

Result<std::string> MakeError() {
  return make_result::Fail(TimedOut());
}

////////////////////////////////////////////////////////////////////////////////

TEST(Result, Ok) {
  static const std::string kMessage = "Hello";

  auto result = Result<TestClass>::Ok(kMessage);
  ASSERT_TRUE(result.IsOk());
  ASSERT_TRUE(result);
  ASSERT_FALSE(result.HasError());
  result.ThrowIfError();  // Nothing happens
  result.ExpectOk();  // Nothing happens

  ASSERT_EQ(result.ValueUnsafe().Message(), kMessage);
  ASSERT_EQ((*result).Message(), "Hello");

  ASSERT_EQ(result->Message(), kMessage);
}

TEST(Result, ObjectCount) {
  {
    auto result = Result<TestClass>::Ok("Hi");
    ASSERT_EQ(TestClass::ObjectCount(), 1);
  }
  ASSERT_EQ(TestClass::ObjectCount(), 0);

  {
    auto result = Result<TestClass>::Fail(TimedOut());
    ASSERT_EQ(TestClass::ObjectCount(), 0);
  }
  ASSERT_EQ(TestClass::ObjectCount(), 0);
}

TEST(Result, OkCopyCtor) {
  static const std::string kMessage = "Copy me";

  auto result = Result<TestClass>::Ok(TestClass(kMessage));
  ASSERT_TRUE(result.IsOk());
  ASSERT_EQ(result->Message(), kMessage);
}

TEST(Result, Error) {
  auto result = Result<TestClass>::Fail(TimedOut());

  ASSERT_FALSE(result.IsOk());
  ASSERT_TRUE(!result);
  ASSERT_TRUE(result.HasError());

  auto error = result.Error();
  ASSERT_EQ(error.value(), (int)std::errc::timed_out);

  ASSERT_THROW(result.ThrowIfError(), std::system_error);
}

TEST(Result, MatchErrorCode) {
  Result<void> result = make_result::Fail(TimedOut());
  ASSERT_TRUE(result.MatchErrorCode((int)std::errc::timed_out));
}

TEST(Result, Move) {
  auto result_1 = Result<TestClass>::Ok("Hello");
  auto result_2 = std::move(result_1);
  ASSERT_EQ(result_2.Value().Message(), "Hello");
}

TEST(Result, Copy) {
  auto result_1 = Result<TestClass>::Ok("Hello");
  Result<TestClass> result_2 = result_1;
  ASSERT_EQ(result_1.Value().Message(), "Hello");
}

TEST(Result, AccessMethods) {
  auto result = Result<TestClass>::Ok("Hello");
  ASSERT_EQ(result->Message(), "Hello");

  const TestClass& test = *result;
  ASSERT_EQ(test.Message(), "Hello");

  TestClass thief = std::move(*result);
  ASSERT_EQ(thief.Message(), "Hello");

  ASSERT_EQ(result.Value().Message(), "");
}

TEST(Result, Void) {
  // Ok
  auto result = Result<void>::Ok();
  ASSERT_FALSE(result.HasError());
  ASSERT_TRUE(result.IsOk());
  ASSERT_TRUE(result);
  result.ThrowIfError();  // Nothing happens
  result.ExpectOk();  // Nothing happens

  // Fail
  auto err_result = Result<void>::Fail(TimedOut());
  ASSERT_TRUE(err_result.HasError());
  ASSERT_TRUE(!err_result);
  ASSERT_THROW(err_result.ThrowIfError(), std::system_error);
  ASSERT_THROW(err_result.ExpectOk(), std::system_error);
  auto error = err_result.Error();
  ASSERT_EQ(error.value(), (int)std::errc::timed_out);
}

TEST(Result, AutomaticallyUnwrapRvalue) {
  std::vector<int> ints = MakeVector(3);
  ASSERT_EQ(ints.size(), 3u);

  auto result_1 = MakeVector(4);
  ints = std::move(result_1);
  ASSERT_EQ(ints.size(), 4);

  auto result_2 = MakeVector(5);
  // Does not compiled
  // ints = result_2;

  std::string str;
  ASSERT_THROW(str = MakeError(), std::system_error);
}

TEST(Result, MakeOkResult) {
  auto ok = make_result::Ok();
  ok.ExpectOk();
  ASSERT_TRUE(ok);

  const size_t answer = 4;
  Result<size_t> result = make_result::Ok(answer);
}

TEST(Result, MakeErrorResult) {
  Result<std::string> response = make_result::Fail(TimedOut());
  ASSERT_FALSE(response);

  Result<std::vector<std::string>> lines = make_result::PropagateError(response);
  ASSERT_FALSE(lines);
}

TEST(Result, MakeResultStatus) {
  auto result = make_result::ToStatus(TimedOut());
  ASSERT_FALSE(result);
  ASSERT_TRUE(result.HasError());
}
