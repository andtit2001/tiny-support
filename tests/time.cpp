#include <gtest/gtest.h>

#include <tinysupport/time.hpp>

#include <thread>

using namespace tinyfiber;
using namespace std::chrono_literals;

TEST(Time, StopWatch) {
  StopWatch stop_watch;
  std::this_thread::sleep_for(1s);
  ASSERT_TRUE(stop_watch.Elapsed() >= 1s);
  ASSERT_TRUE(stop_watch.Elapsed() < 1s + 100ms);
}
