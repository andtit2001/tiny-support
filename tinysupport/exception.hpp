#pragma once

#include <string>

namespace tinyfiber {

std::string CurrentExceptionMessage();

}  // namespace tinyfiber
