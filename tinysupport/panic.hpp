#pragma once

#include <tinysupport/string_builder.hpp>

namespace tinyfiber {
namespace detail {
void Panic(const std::string& error);
}  // namespace detail
}  // namespace tinyfiber

// Print error message to stderr, then abort
// Usage: TINY_PANIC("Internal error: " << e.what());

#define TINY_PANIC(error)                                                  \
  do {                                                                     \
    tinyfiber::detail::Panic(tinyfiber::StringBuilder() << ": " << error); \
  } while (false)
