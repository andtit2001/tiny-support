#include <tinysupport/result.hpp>

namespace tinyfiber {

namespace make_result {

Result<void> Ok() {
  return Result<void>::Ok();
}

detail::Failure Fail(std::error_code error) {
  TINY_VERIFY(error, "Expected error");
  return detail::Failure{error};
}

Status ToStatus(std::error_code error) {
  if (error) {
    return Fail(error);
  } else {
    return Ok();
  }
}

}  // namespace make_result

}  // namespace tinyfiber
