#include <tinysupport/exception.hpp>

#include <tinysupport/panic.hpp>

#include <stdexcept>

namespace tinyfiber {

std::string CurrentExceptionMessage() {
  auto current = std::current_exception();

  if (!current) {
    TINY_PANIC("Not in exception context");
  }

  try {
    std::rethrow_exception(current);
  } catch (std::exception& e) {
    return {e.what()};
  } catch (...) {
    return "Unknown exception";
  }
}

}  // namespace tinyfiber
